package io.github.robertograham.fortnite2.domain;

import io.github.robertograham.fortnite2.domain.enumeration.Platform;

import java.util.Map;

/**
 * Representation of an Epic Games account
 *
 * @since 1.0.0
 */
public interface Account {

    /**
     * @return ID of this Epic Games account
     * @since 1.0.0
     */
    String accountId();

    /**
     * @return username of this Epic Games account
     * @since 1.0.0
     */
    String displayName();

    /**
     * @return external names of connected platforms of this Epic Games account
     */
    Map<Platform, String> externalNames();
}
