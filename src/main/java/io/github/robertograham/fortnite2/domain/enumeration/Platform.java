package io.github.robertograham.fortnite2.domain.enumeration;

/**
 * Platform a game of Fortnite can take place on
 */
public enum Platform {

    PC("pc"), PS4("ps4"), XB1("xb1");

    private final String code;

    Platform(final String code) {
        this.code = code;
    }

    public String code() {
        return code;
    }

    public static Platform ofCode(String code) {
        switch (code) {
            case "pc":
                return PC;
            case "ps4":
            case "psn":
                return PS4;
            case "xb1":
            case "xbl":
                return XB1;
        }
        return null;
    }
}
