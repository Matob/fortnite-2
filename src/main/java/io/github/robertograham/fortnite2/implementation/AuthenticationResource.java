package io.github.robertograham.fortnite2.implementation;

import com.jsoniter.JsonIterator;
import io.github.robertograham.fortnite2.domain.Token;
import okhttp3.*;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.EntityBuilder;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.riversun.okhttp3.OkHttp3CookieHelper;

import java.io.IOException;
import java.io.StringBufferInputStream;
import java.io.StringReader;
import java.net.CookieManager;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.HashMap;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.apache.http.HttpHeaders.AUTHORIZATION;
import static org.apache.http.entity.ContentType.APPLICATION_FORM_URLENCODED;

@SuppressWarnings("Duplicates")
final class AuthenticationResource {
    public static final MediaType URLENCODED
            = MediaType.parse("application/x-www-form-urlencoded");
    public static final MediaType JSON
            = MediaType.parse("application/json; charset=utf-8");

    private static final NameValuePair GRANT_TYPE_EXCHANGE_CODE_PARAMETER = new BasicNameValuePair("grant_type", "exchange_code");
    private static final NameValuePair GRANT_TYPE_REFRESH_TOKEN_PARAMETER = new BasicNameValuePair("grant_type", "refresh_token");
    private static final NameValuePair GRANT_TYPE_OTP_PARAMETER = new BasicNameValuePair("grant_type", "otp");
    private static final NameValuePair TOKEN_TYPE_EG1 = new BasicNameValuePair("token_type", "eg1");
    private final CloseableHttpClient httpClient;
    private final OptionalResponseHandlerProvider optionalResponseHandlerProvider;
    private final OkHttpClient okHttpClient;

    private AuthenticationResource(final CloseableHttpClient httpClient,
                                   final OptionalResponseHandlerProvider optionalResponseHandlerProvider) {
        this.httpClient = httpClient;
        this.optionalResponseHandlerProvider = optionalResponseHandlerProvider;

        var b = new OkHttpClient.Builder();
        b.connectTimeout(30, TimeUnit.SECONDS);
        b.readTimeout(30, TimeUnit.SECONDS);
        b.writeTimeout(30, TimeUnit.SECONDS);
        var c = b.build();
        c.dispatcher().setMaxRequestsPerHost(100);
        c.dispatcher().setMaxRequests(200);
        okHttpClient = b.build();
    }

    static AuthenticationResource newInstance(final CloseableHttpClient httpClient,
                                              final OptionalResponseHandlerProvider optionalResponseHandlerProvider) {
        return new AuthenticationResource(
                httpClient,
                optionalResponseHandlerProvider
        );
    }

    private Optional<Token> postForToken(final String bearerToken,
                                         final NameValuePair... formParameters) throws IOException {
        return httpClient.execute(
                RequestBuilder.post("https://account-public-service-prod03.ol.epicgames.com/account/api/oauth/token")
                        .setHeader(AUTHORIZATION, "basic " + bearerToken)
                        .setEntity(EntityBuilder.create()
                                .setContentType(APPLICATION_FORM_URLENCODED)
                                .setParameters(formParameters)
                                .build())
                        .build(),
                optionalResponseHandlerProvider.forClass(DefaultToken.class)
        )
                .map(Function.identity());
    }

    Optional<Token> passwordGrantedToken(final String epicGamesEmailAddress,
                                         final String epicGamesPassword,

                                         final String epicGamesLauncherToken) throws IOException {
        var cookieMap = new HashMap<String, String>();
        String arStr;
        String csrf;

        do {
            //System.out.println("CookieMap=" + cookieMap);
            var csrfResponse = okHttpClient.newCall(new Request.Builder()
                    .url("https://www.epicgames.com/id/api/csrf")
                    .header("Cookie", cookieMap.entrySet().stream().map(e -> e.getKey() + "=" + e.getValue()).collect(Collectors.joining("; ")))
                    .get()
                    .build()).execute();
            csrfResponse.headers("Set-Cookie").stream().map(str -> str.substring(0, str.indexOf(";")))
                    .forEach(str -> cookieMap.put(str.split("=")[0], str.split("=")[1].split(";")[0]));
            //System.out.println("CookieMap=" + cookieMap);

            csrf = csrfResponse.headers("Set-Cookie")
                    .stream()
                    .filter(c -> c.startsWith("XSRF-TOKEN"))
                    .map(c -> c.substring("XSRF-TOKEN=".length()).replace("; Path=/id", ""))
                    .findAny().orElseThrow();

            //System.out.println("CSRF:" + csrf);
            var cookies1 = csrfResponse.headers("Set-Cookie")
                    .stream().map(str -> str.substring(0, str.indexOf(";"))).collect(Collectors.toList());
            csrfResponse.headers("Set-Cookie").stream().map(str -> str.substring(0, str.indexOf(";")))
                    .forEach(str -> cookieMap.put(str.split("=")[0], str.split("=").length > 1 ? str.split("=")[1].split(";")[0] : ""));
            //System.out.println("CookieMap=" + cookieMap);

            //System.out.println("Sending login with: " + cookieMap.entrySet().stream().map(e -> e.getKey() + "=" + e.getValue()).collect(Collectors.joining("; ")));
            var aResponse = okHttpClient.newCall(new Request.Builder()
                    .url("https://www.epicgames.com/id/api/login")
                    //  .header("XSRF-TOKEN", csrf)
                    .header("Content-Type", "application/json;charset=UTF-8")
                    .header("Cookie", cookieMap.entrySet().stream().map(e -> e.getKey() + "=" + e.getValue()).collect(Collectors.joining("; ")))
                    .header("X-XSRF-TOKEN", csrf)
                    .post(RequestBody.create(JSON, "{\"email\":\"" + epicGamesEmailAddress +
                            "\",\"password\":\"" + epicGamesPassword + "\",\"rememberMe\":false}")
                    ).build()).execute();
            //System.out.println("CookieMap=" + cookieMap);

            arStr = aResponse.body().string();
            //System.out.println("LOGIN RESPONSE:" + arStr);

            var cookies = aResponse.headers("Set-Cookie")
                    .stream().map(str -> str.substring(0, str.indexOf(";"))).collect(Collectors.toList());
            //cookies.forEach(System.out::println);
            aResponse.headers("Set-Cookie").stream().map(str -> str.substring(0, str.indexOf(";")))
                    .forEach(str -> cookieMap.put(str.split("=")[0], str.split("=").length > 1 ? str.split("=")[1].split(";")[0] : ""));
            csrf = cookies.stream().filter(c -> c.startsWith("XSRF-TOKEN")).map(c -> c.split("=")[1]).findAny().orElse(csrf);
        } while(arStr.contains("errors.com.epicgames.accountportal.session_invalidated"));

        var loginResponse = okHttpClient.newCall(new Request.Builder()
                .url("https://www.epicgames.com/id/api/redirect")
                .get()
                .header("x-xsrf-token", csrf)
                .header("Cookie", cookieMap.entrySet().stream().map(e -> e.getKey() + "=" + e.getValue()).collect(Collectors.joining("; ")))
                .header("Referer", "https://www.epicgames.com/id/login")
                .build()).execute().body().string();
        //System.out.println("REDIRECT RESPONSE:" + loginResponse);

        okHttpClient.newCall(new Request.Builder()
                .url("https://www.epicgames.com/id/api/authenticate")
                .header("Cookie", cookieMap.entrySet().stream().map(e -> e.getKey() + "=" + e.getValue()).collect(Collectors.joining("; ")))
                .get()
                .build()).execute();

        var exchangeResponse = okHttpClient.newCall(new Request.Builder()
                .url("https://www.epicgames.com/id/api/exchange")
                .header("Cookie", cookieMap.entrySet().stream().map(e -> e.getKey() + "=" + e.getValue()).collect(Collectors.joining("; ")))
                .get()
                .header("x-xsrf-token", csrf)
                .build()).execute().body().string();

        //System.out.println("EXCHANGE RESPONSE:" + exchangeResponse);


        var code = JsonIterator.deserialize(exchangeResponse).get("code").toString();

        var tokenResponse = okHttpClient.newCall(new Request.Builder()
                .url("https://account-public-service-prod03.ol.epicgames.com/account/api/oauth/token")
                .header("x-xsrf-token", csrf)
                .header("Authorization", "basic MzQ0NmNkNzI2OTRjNGE0NDg1ZDgxYjc3YWRiYjIxNDE6OTIwOWQ0YTVlMjVhNDU3ZmI5YjA3NDg5ZDMxM2I0MWE=")
                .post(new FormBody.Builder()
                        .add("grant_type", "exchange_code")
                        .add("exchange_code", code)
                        .add("includePerms", "" + false)
                        .add("token_type", "eg1")
                        .build()).build()).execute().body().string();

        //System.out.println("TOKEN RESPONSE:" + tokenResponse);


        var json = JsonIterator.deserialize(tokenResponse);

        return Optional.of(new DefaultToken(json.get("access_token").toString(),
                LocalDateTime.ofInstant(Instant.parse(json.get("expires_at").toString()),
                        ZoneOffset.UTC),
                json.get("refresh_token").toString(),
                LocalDateTime.ofInstant(Instant.parse(json.get("refresh_expires_at").toString()),
                        ZoneOffset.UTC),
                json.get("in_app_id").toString(),
                json.get("account_id").toString()));
    }

    Optional<Exchange> accessTokenGrantedExchange(final String accessToken) throws IOException {
        return httpClient.execute(
                RequestBuilder.get("https://account-public-service-prod03.ol.epicgames.com/account/api/oauth/exchange")
                        .setHeader(AUTHORIZATION, "bearer " + accessToken)
                        .build(),
                optionalResponseHandlerProvider.forClass(Exchange.class)
        );
    }

    Optional<Token> exchangeCodeGrantedToken(final String exchangeCode,
                                             final String fortniteClientToken) throws IOException {
        return postForToken(
                fortniteClientToken,
                GRANT_TYPE_EXCHANGE_CODE_PARAMETER,
                TOKEN_TYPE_EG1,
                new BasicNameValuePair("exchange_code", exchangeCode)
        );
    }

    Optional<Token> refreshTokenGrantedToken(final String refreshToken,
                                             final String fortniteClientToken) throws IOException {
        return postForToken(
                fortniteClientToken,
                GRANT_TYPE_REFRESH_TOKEN_PARAMETER,
                new BasicNameValuePair("refresh_token", refreshToken)
        );
    }

    Optional<Token> twoFactorAuthenticationCodeGrantedToken(final String epicGamesLauncherToken,
                                                            final String challenge,
                                                            final String twoFactorAuthenticationCode) throws IOException {
        return postForToken(
                epicGamesLauncherToken,
                GRANT_TYPE_OTP_PARAMETER,
                new BasicNameValuePair("otp", twoFactorAuthenticationCode),
                new BasicNameValuePair("challenge", challenge)
        );
    }

    void retireAccessToken(final String accessToken) throws IOException {
        httpClient.execute(
                RequestBuilder.delete("https://account-public-service-prod03.ol.epicgames.com/account/api/oauth/sessions/kill/" + accessToken)
                        .setHeader(AUTHORIZATION, "bearer " + accessToken)
                        .build(),
                optionalResponseHandlerProvider.forString()
        );
    }

    Optional<Eula> getEula(final String accessToken, final String accountId) throws IOException {
        return httpClient.execute(
                RequestBuilder.get(String.format(
                        "%s/%s",
                        "https://eulatracking-public-service-prod-m.ol.epicgames.com/eulatracking/api/public/agreements/fn/account",
                        accountId
                ))
                        .setHeader(AUTHORIZATION, "bearer " + accessToken)
                        .addParameter("locale", "en-US")
                        .build(),
                optionalResponseHandlerProvider.forClass(Eula.class)
        );
    }

    void acceptEula(final String accessToken, final String accountId, final long eulaVersion) throws IOException {
        httpClient.execute(
                RequestBuilder.post(String.format(
                        "%s/%d/%s/%s/%s",
                        "https://eulatracking-public-service-prod-m.ol.epicgames.com/eulatracking/api/public/agreements/fn/version",
                        eulaVersion,
                        "account",
                        accountId,
                        "accept"
                ))
                        .setHeader(AUTHORIZATION, "bearer " + accessToken)
                        .addParameter("locale", "en")
                        .build(),
                optionalResponseHandlerProvider.forString()
        );
    }

    void grantAccess(final String accessToken, final String accountId) throws IOException {
        httpClient.execute(
                RequestBuilder.post(String.format(
                        "%s/%s",
                        "https://fortnite-public-service-prod11.ol.epicgames.com/fortnite/api/game/v2/grant_access",
                        accountId
                ))
                        .setHeader(AUTHORIZATION, "bearer " + accessToken)
                        .build(),
                optionalResponseHandlerProvider.forString()
        );
    }

    void killOtherSessions(final String accessToken) throws IOException {
        httpClient.execute(
                RequestBuilder.delete("https://account-public-service-prod03.ol.epicgames.com/account/api/oauth/sessions/kill")
                        .addParameter("killType", "OTHERS_ACCOUNT_CLIENT_SERVICE")
                        .setHeader(AUTHORIZATION, "bearer " + accessToken)
                        .build(),
                optionalResponseHandlerProvider.forString()
        );
    }
}
